﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipManager : MonoBehaviour {
	
	public int health = 30;
	private UnityEngine.UI.Text shipHealthUI;
	private UnityEngine.UI.RawImage gameOver;

	// Use this for initialization
	void Start () {
		shipHealthUI = GameObject.Find("PlayerHealth").GetComponent<UnityEngine.UI.Text>();
		gameOver = GameObject.Find ("LoseScreen").GetComponent<UnityEngine.UI.RawImage> ();
		shipHealthUI.text = health.ToString ();
	}
	
	// Update is called once per frame
	void Update () {
		// transform.position = new Vector3 (Mathf.PingPong (Time.time * 2, max - min) + min, transform.position.y, transform.position.z);

	}

	public void DamageShip() {
		health -= 3;
		shipHealthUI.text = health.ToString ();
		if (health <= 0) {
			gameOver.enabled = true;
			Invoke ("RestartScene", 3.0f);
		}
	}

	void RestartScene() {
		Application.LoadLevel (Application.loadedLevel);
	}
}