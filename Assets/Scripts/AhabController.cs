﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AhabController : MonoBehaviour {

	public float inputDelay = 0.1f;
	public float speed = 3;
	public float rotateSpeed = 100;
	public float gravity = 20.0f;

	CharacterController cController;
	private float verticalInput, horizontalInput;
	private Vector3 moveDirection = Vector3.zero;
	private Vector3 input = Vector3.zero;
	private Vector3 targetRotation = Vector3.zero;
	private GameObject cannon = null;
	Animation anim;

	// Use this for initialization
	void Start () {
		cController = GetComponent<CharacterController> ();
		targetRotation = transform.rotation.eulerAngles;
		anim = GetComponent<Animation>();

		verticalInput = 0;
		horizontalInput = 0;
	}

	// Update is called once per frame
	void Update () {
		GetInput ();
	}

	void OnTriggerStay(Collider hit) {
		if (hit.tag == "cannon" && Input.GetKeyDown(KeyCode.Space)) {
			cannon = hit.gameObject;
			CannonController cannonScript = cannon.GetComponent<CannonController> ();
			cannonScript.isFiring = true;
		}
	}

	void GetInput() {
		verticalInput = Input.GetAxis ("Vertical");
		horizontalInput = Input.GetAxis ("Horizontal");
		input = new Vector3 (horizontalInput, 0f, verticalInput);

		if (input.sqrMagnitude > 1f)
			input.Normalize ();

		if (input != Vector3.zero) {
			targetRotation = Quaternion.LookRotation (input).eulerAngles;
			anim.Play ("AhabWalk");
		} else {
			anim.Stop ("AhabWalk");
		}
	}

	void FixedUpdate() {
		Run ();
	}

	void Run() {
		if (cController.isGrounded) {
			moveDirection = new Vector3 (horizontalInput, 0f, verticalInput);
			moveDirection *= speed;
		} 

		transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.Euler (targetRotation.x, Mathf.Round (targetRotation.y / 45) * 45, targetRotation.z), Time.deltaTime * rotateSpeed);
		moveDirection.y -= gravity * Time.deltaTime;
		cController.Move(moveDirection * Time.deltaTime);
	}
}
