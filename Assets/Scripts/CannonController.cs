﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonController : MonoBehaviour {
	public bool isFiring = false;
	public GameObject cannonBallPrefab;
	private Vector3 cannonBallLocation;
	private Quaternion cannonBallRotation;

	// Use this for initialization
	void Start () {
		if(transform.position.x > 0)
			cannonBallLocation = new Vector3 (transform.position.x + 1.3f, transform.position.y + .3f, transform.position.z + 0.3f);
		else
			cannonBallLocation = new Vector3 (transform.position.x - 1f, transform.position.y + .3f, transform.position.z + 1f);
		cannonBallRotation = transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
		if (isFiring) {
			// fire cannonBall
			isFiring = false;
			var cannonBall = GameObject.Instantiate (
				cannonBallPrefab,
				cannonBallLocation,
				transform.rotation
			                 );

			cannonBall.GetComponent<Rigidbody> ().velocity = cannonBall.transform.forward * -12;

			Destroy (cannonBall, 2.0f);
		}
	}
}