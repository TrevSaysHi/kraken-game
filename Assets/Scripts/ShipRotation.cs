﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipRotation : MonoBehaviour {

	private float speed = 2f;
	private float max = 5f;
	private float maxRotation = 2f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.rotation = Quaternion.Euler(maxRotation * Mathf.Sin(Time.time * speed), transform.rotation.eulerAngles.y, 0f);
	}
}
