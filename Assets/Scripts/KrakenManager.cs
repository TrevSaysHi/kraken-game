﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Top Right Coordinates x = 6 y = -1 z = 2
// Top Right Rotation x = -90 y = 0 z = -135
// Bottom Right Coordinates x = 8 y = -1 z = -.3
// Bottom Right Rotation x = -90 y = 0 z = -118
// Top Left Coordinates x = -6.5 y = -1 z = 1.5
// Top Left Rotation x = -90 y = 0 z = 135
// Bottom Left Coordinates x = -7.4 y = -1 z = -1.25
// Bottom Left Rotation x = -90 y = 0 z = 118

public class KrakenManager : MonoBehaviour {
	public bool TRTentacle = false;
	public bool BRTentacle = false;
	public bool TLTentacle = false;
	public bool BLTentacle = false;
	public GameObject krakenTentacle;
	private UnityEngine.UI.Text krakenHealthUI;
	public int krakenHealth = 75;
	public float tentacleSpeed = 5.0f;

	private Vector3 TRTentaclePosition = new Vector3 (6f, -7f, 2f);
	private Vector3 BRTentaclePosition = new Vector3 (8f, -7f, -.3f);
	private Vector3 TLTentaclePosition = new Vector3 (-6.5f, -7f, 1.5f);
	private Vector3 BLTentaclePosition = new Vector3 (-7.4f, -7f, -1.25f);
	private float randomNum;
	public bool isAlive = true;
	private bool spawning = true;

	private UnityEngine.UI.RawImage youWin;

	// Use this for initialization
	void Start () {
		Invoke ("SpawnTentacles", tentacleSpeed);
		krakenHealthUI = GameObject.Find("EnemyHealth").GetComponent<UnityEngine.UI.Text>();
		youWin = GameObject.Find ("WinScreen").GetComponent<UnityEngine.UI.RawImage> ();
		krakenHealthUI.text = krakenHealth.ToString();
	}
	
	// Update is called once per frame
	void Update () {
		CheckForDeath ();
	}

	public void DamageKraken() {
		krakenHealth -= 5;

		if (krakenHealth == 50 || krakenHealth == 25 || krakenHealth == 10) {
			tentacleSpeed -= 1.0f;
		}
		krakenHealthUI.text = krakenHealth.ToString();
	}

	void CheckForDeath() {
		if (krakenHealth <= 0) {
			isAlive = false;
			youWin.enabled = true;
			Invoke ("RestartScene", 3.0f);
		}
	}

	void SpawnTentacles() {
		randomNum = Random.Range (0, 4);

		if (isAlive) {
			if (randomNum == 0 && !TRTentacle) {
				TRTentacle = true;
				var tentacle = GameObject.Instantiate (krakenTentacle, TRTentaclePosition, Quaternion.identity);
				KrakenTentacle tentInstance = tentacle.GetComponent<KrakenTentacle> ();
				tentInstance.isTopRight = true;
			} else if (randomNum == 1 && !BRTentacle) {
				BRTentacle = true;
				var tentacle = GameObject.Instantiate (krakenTentacle, BRTentaclePosition, Quaternion.identity);
				KrakenTentacle tentInstance = tentacle.GetComponent<KrakenTentacle> ();
				tentInstance.isBotRight = true;
			} else if (randomNum == 2 && !TLTentacle) {
				TLTentacle = true;
				var tentacle = GameObject.Instantiate (krakenTentacle, TLTentaclePosition, Quaternion.identity);
				KrakenTentacle tentInstance = tentacle.GetComponent<KrakenTentacle> ();
				tentInstance.isTopLeft = true;
			} else if (randomNum == 3 && !BLTentacle) {
				BLTentacle = true;
				var tentacle = GameObject.Instantiate (krakenTentacle, BLTentaclePosition, Quaternion.identity);
				KrakenTentacle tentInstance = tentacle.GetComponent<KrakenTentacle> ();
				tentInstance.isBotLeft = true;
			}
			Invoke ("SpawnTentacles", tentacleSpeed);
		}
	}

	void RestartScene() {
		Application.LoadLevel (Application.loadedLevel);
	}
}
