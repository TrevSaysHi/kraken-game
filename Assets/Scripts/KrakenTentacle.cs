﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KrakenTentacle : MonoBehaviour {
	public bool isTopRight = false;
	public bool isBotRight = false;
	public bool isTopLeft = false;
	public bool isBotLeft = false;
	public float speed = 3f;
	private bool rising = true;
	private Vector3 properHeight;
	private Vector3 sinkHeight;
	private float randomNum;
	KrakenManager krakenParent;
	ShipManager ship;

	// Use this for initialization
	void Start () {
		RotateProperly ();
		krakenParent = GameObject.Find("TheKraken").GetComponent<KrakenManager>();
		ship = GameObject.Find("pirate_ship").GetComponent<ShipManager>();
		properHeight = new Vector3 (transform.position.x, -1f, transform.position.z);
		sinkHeight = new Vector3 (transform.position.x, -7.5f, transform.position.z);

		randomNum = Random.Range (3, 7);
		Invoke ("AttackShip", randomNum);
	}

	void RotateProperly() {
		if (isTopRight) {
			transform.Rotate (0f, -135f, 0f);
		} else if (isBotRight) {
			transform.Rotate (0f, -118f, 0f);
		} else if (isTopLeft) {
			transform.Rotate (0f, 135f, 0f);
		} else if (isBotLeft) {
			transform.Rotate (0f, 118f, 0f);
		}
	}

	void OnTriggerEnter(Collider hit) {
		// Play hurt animation
		if (hit.tag == "cannonBall" && rising) {
			UpdateKraken ();
			krakenParent.DamageKraken();
			Destroy (hit.gameObject);
			rising = false;
		}
	}

	void AttackShip() {
		if (rising) {
			ship.DamageShip ();
		}
		rising = false;
	}

	void DestroySelf() {
		Destroy (this.gameObject);
	}

	void UpdateKraken() {
		if (isTopRight) {
			krakenParent.TRTentacle = false;
		} else if (isBotRight) {
			krakenParent.BRTentacle = false;
		} else if (isTopLeft) {
			krakenParent.TLTentacle = false;
		} else if (isBotLeft) {
			krakenParent.BLTentacle = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(!krakenParent.isAlive) {
			rising = false;
		}
		if (transform.position.y < -1 && rising) {
			Vector3 visibleHeight = Vector3.MoveTowards (transform.position, properHeight, speed * Time.deltaTime);
			transform.position = visibleHeight;
		} 
		if (!rising) {
			Vector3 visibleHeight = Vector3.MoveTowards (transform.position, sinkHeight, speed * Time.deltaTime);
			transform.position = visibleHeight;
		}
		if (transform.position.y == -7.5f) {
			UpdateKraken ();
			DestroySelf ();
		}
	}
}
